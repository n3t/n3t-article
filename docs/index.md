n3t Article
===========

![Version](https://img.shields.io/badge/Latest%20version-5.0.1-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--02--22-informational)

n3t Article module for Joomla! provides simple way, how to display an article on module 
position.
