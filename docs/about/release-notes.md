Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased]
------------

- no unreleased changes

[5.0.x]
-------

### [5.0.1] - 2024-02-22

#### Fixed
- Joomla 3 PHP warnings.

### [5.0.0] - 2024-02-15

#### Changed
- Improved installer
#### Added
- Joomla 5 compatibility

[4.0.x]
-------

### [4.0.3] - 2023-02-02

#### Fixed
- PHP 8.2 warning

### [4.0.2] - 2022-12-16

#### Changed
- ignore 404 errors during article loading (when unpublished, article will just not appear) 

### [4.0.1] - 2022-10-17

#### Fixed
- correct field type to select article for Joomla 4
#### Changed
- no more using module class sfx in default template, as it is used in module chrome already (Joomla 4 style)

### [4.0.0] - 2022-10-17

#### Fixed
- Check Show readmore parameter
#### Added
- Joomla 4 compatibility
#### Removed
- Print and email icon settings
#### Changed
- improved installer

[3.0.x]
-------

### [3.0.7] - 2018-03-23

#### Fixed
- Corrected parameters rewrite system
#### Added
- Parameter for article header tag

### [3.0.6] - 2018-03-16

#### Added
- Article options are rewritable in module settings now
#### Changed
- Updated template to match Joomla 3.8

### [3.0.5] - 2017-07-13

#### Added
- Installer script to remove old update site

### [3.0.4] - 2017-07-13

#### Changed
- New update site

### [3.0.3] - 2017-04-22

#### Fixed
- Fixed JHtml::icon not found

### [3.0.2] - 2016-07-22

#### Changed
- Code improvements

### [3.0.1] - 2016-04-19

#### Changed
- Code improvements

### [3.0.0] - 2015-08-14

- Initial release
