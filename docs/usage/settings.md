Settings
========

Basic settings
--------------

#### Article

Select an article to display. This is required settings.

#### Article header tag

Select HTML tag used to render Article Title.

Article settings
----------------

n3t Article follow article settings y default. So, for example, if you don't want to display
the title of article, go to article manager, edit article and on its Options tab
set Show title to Hide.

If article contains Read More separator, only introtext (the part before the separator)
is displayed in module, followed by Readmore link (when readmore enabled).

__However__ It is possible to overwrite articles settings in module settings. For example, 
if you don't want to show title in module, but in article itself yes, set this in module 
instead of article settings.