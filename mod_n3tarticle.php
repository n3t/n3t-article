<?php
/**
 * @package n3t Article
 * @author Pavel Poles - n3t.cz
 * @copyright (c) 2012 - 2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// no direct access
defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Helper\ModuleHelper;
use Joomla\CMS\Version;

require_once __DIR__ . '/helper.php';

/** @var \Joomla\Registry\Registry $params */
/** @var \stdClass $module */

$article_id = $params->get('articleid');

$cacheid = md5(serialize([$article_id, $module->module]));

$cacheparams               = new stdClass;
$cacheparams->cachemode    = 'id';
$cacheparams->class        = 'modN3tArticleHelper';
$cacheparams->method       = 'getArticle';
$cacheparams->methodparams = $params;
$cacheparams->modeparams   = $cacheid;

if (Version::MAJOR_VERSION >= 4)
	Factory::getApplication()->getLanguage()->load('com_content');
else {
	Factory::getLanguage()->load('com_content');
	HtmlHelper::addIncludePath(JPATH_BASE . '/components/com_content/helpers');
}

$item = ModuleHelper::moduleCache($module, $params, $cacheparams);

if (!empty($item))
{
  $moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx', ''));
  require ModuleHelper::getLayoutPath('mod_n3tarticle', $params->get('layout', 'default'));
}
