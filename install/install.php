<?php
/**
 * @package n3t Article
 * @author Pavel Poles - n3t.cz
 * @copyright (c) 2012 - 2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Installer\InstallerScript;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Version;

class mod_N3tArticleInstallerScript extends InstallerScript
{
  const LANG_PREFIX = 'MOD_N3TARTICLE';
  protected $minimumJoomla = '3.10.0';
  protected $minimumPhp = '7.4.0';
  protected $allowDowngrades = true;
  protected $oldRelease = null;

  /**
   * @inheritdoc
   *
   * @since 4.0.0
   */
  public function preflight($type, $parent)
  {
    $return = parent::preflight($type, $parent);

    if (strtolower($type) === 'update') {
      if (Version::MAJOR_VERSION === 3) {
        $db = Factory::getDbo();
        $manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $db->quote($this->extension));
      } else {
        $manifest = $this->getItemArray('manifest_cache', '#__extensions', 'name', $this->extension);
      }

      if (isset($manifest['version'])) {
        $this->oldRelease = $manifest['version'];
      }
    }

    return $return;
  }

  /**
   * @inheritdoc
   *
   * @since 4.0.0
   */
  public function update($parent)
  {
    if ($this->oldRelease) {
      Factory::getApplication()->enqueueMessage(Text::sprintf(self::LANG_PREFIX . '_INSTALL_UPDATE_VERSION', $this->oldRelease, $this->release));
    }
  }

}

