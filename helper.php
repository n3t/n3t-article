<?php
/**
 * @package n3t Article
 * @author Pavel Poles - n3t.cz
 * @copyright (c) 2012 - 2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

// no direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Helper\TagsHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Version;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\Event\Event;
use Joomla\Registry\Registry;
use Joomla\CMS\Event\Content;
use Joomla\Component\Content\Site\Helper\RouteHelper;

if (!class_exists('\\Joomla\\Component\\Content\\Site\\Helper\\RouteHelper') && Version::MAJOR_VERSION === 3) {
  require_once JPATH_ROOT . '/components/com_content/helpers/route.php';
  class_alias('ContentHelperRoute', '\\Joomla\\Component\\Content\\Site\\Helper\\RouteHelper');
}

abstract class modN3tArticleHelper
{

  public static function getArticle(&$params)
  {
    /** @var \Joomla\Component\Content\Site\Model\ArticleModel $model */

    $articleid = $params->get('articleid');

    $app = Factory::getApplication();
    if (Version::MAJOR_VERSION >= 4) {
      $user = $app->getIdentity();
      $factory = $app->bootComponent('com_content')->getMVCFactory();
      $model = $factory->createModel('Article', 'Site', ['ignore_request' => true]);
      $appParams = $app->getParams();
    } else {
      require_once JPATH_SITE.'/components/com_content/helpers/route.php';
      jimport('joomla.application.component.model');
      JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');

      $user = Factory::getUser();
      $model = JModelLegacy::getInstance('Article', 'ContentModel', array('ignore_request' => true));
      $appParams = Factory::getApplication()->getParams();
    }

    $model->setState('article.id', $articleid);
    $model->setState('params', $appParams);

    if ((!$user->authorise('core.edit.state', 'com_content')) &&  (!$user->authorise('core.edit', 'com_content'))) {
      $model->setState('filter.published', 1);
      $model->setState('filter.archived', 2);
    }

    // Ignore 404 errors
    try
    {
      if (Version::MAJOR_VERSION === 3) {
        $errorHandling = JError::getErrorHandling(E_ERROR);
        JError::setErrorHandling(E_ERROR, 'ignore');
      }
      try
      {
        $item = $model->getItem();

        if (is_a($item, '\\JException')) {
          return null;
        }

        if ($model->getError()) {
          return null;
        }
      } finally {
        if (Version::MAJOR_VERSION == 3) {
          JError::setErrorHandling(E_ERROR, $errorHandling['mode'], isset($errorHandling['options']) ? $errorHandling['options'] : []);
        }
      }
    } catch (\Exception $e) {
      return null;
    }

    $item->slug = $item->alias ? ($item->id . ':' . $item->alias) : $item->id;
    $item->catslug = $item->category_alias ? ($item->catid . ':' . $item->category_alias) : $item->catid;
    $item->parent_slug = $item->parent_alias ? ($item->parent_id . ':' . $item->parent_alias) : $item->parent_id;
    if ($item->parent_alias == 'root')
      $item->parent_slug = null;

    $tempParams = clone ComponentHelper::getParams('com_content');
    $tempParams->merge($item->params);
    $tempParams->merge($params);
    $item->params = $tempParams;

    $item->readmore = $item->fulltext ? true : false;
    $item->readmore_link = Route::_(RouteHelper::getArticleRoute($item->slug, $item->catid, $item->language));

    $item->text = $item->introtext;

    $item->tags = new TagsHelper();
    $item->tags->getItemTags('com_content.article', $item->id);

    $articleParams = new Registry;
    $articleParams->loadString($item->attribs);

    $item->alternative_readmore = $articleParams->get('alternative_readmore');

    $item->event = new \stdClass;
    if (Version::MAJOR_VERSION >= 5) {
      $dispatcher = $app->getDispatcher();

      PluginHelper::importPlugin('content', null, true, $dispatcher);

      $contentEventArguments = [
        'context' => 'com_content.article',
        'subject' => $item,
        'params'  => $item->params,
        'page'    => 0,
      ];

      $dispatcher->dispatch('onContentPrepare', new Content\ContentPrepareEvent('onContentPrepare', $contentEventArguments));

      $contentEvents = [
        'afterDisplayTitle'    => new Content\AfterTitleEvent('onContentAfterTitle', $contentEventArguments),
        'beforeDisplayContent' => new Content\BeforeDisplayEvent('onContentBeforeDisplay', $contentEventArguments),
        'afterDisplayContent'  => new Content\AfterDisplayEvent('onContentAfterDisplay', $contentEventArguments),
      ];

      foreach ($contentEvents as $resultKey => $event) {
        $results = $dispatcher->dispatch($event->getName(), $event)->getArgument('result', []);

        $item->event->{$resultKey} = $results ? trim(implode("\n", $results)) : '';
      }
    } elseif (Version::MAJOR_VERSION >= 4) {
      PluginHelper::importPlugin('content');

      $dispatcher = $app->getDispatcher();

      $event = new Event('onContentPrepare', ['com_content.article', &$item, &$item->params, 0]);
      $dispatcher->dispatch($event->getName(), $event);

      $results = Factory::getApplication()->triggerEvent('onContentAfterTitle', ['com_content.article', &$item, &$item->params, 0]);
      $item->event->afterDisplayTitle = trim(implode("\n", $results));

      $results = Factory::getApplication()->triggerEvent('onContentBeforeDisplay', ['com_content.article', &$item, &$item->params, 0]);
      $item->event->beforeDisplayContent = trim(implode("\n", $results));

      $results = Factory::getApplication()->triggerEvent('onContentAfterDisplay', ['com_content.article', &$item, &$item->params, 0]);
      $item->event->afterDisplayContent = trim(implode("\n", $results));
    } else {
      PluginHelper::importPlugin('content');

      $dispatcher = JDispatcher::getInstance();

      $dispatcher->trigger('onContentPrepare', ['com_content.article', &$item, &$item->params, 0]);

      $results = $dispatcher->trigger('onContentAfterTitle', ['com_content.article', &$item, &$item->params, 0]);
      $item->event->afterDisplayTitle = trim(implode("\n", $results));

      $results = $dispatcher->trigger('onContentBeforeDisplay', ['com_content.article', &$item, &$item->params, 0]);
      $item->event->beforeDisplayContent = trim(implode("\n", $results));

      $results = $dispatcher->trigger('onContentAfterDisplay', ['com_content.article', &$item, &$item->params, 0]);
      $item->event->afterDisplayContent = trim(implode("\n", $results));
    }

    return $item;
  }
}

