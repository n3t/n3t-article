<?php
/**
 * @package n3t Article
 * @author Pavel Poles - n3t.cz
 * @copyright (c) 2012 - 2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Version;
use Joomla\Component\Content\Site\Helper\RouteHelper;

if (!class_exists('\\Joomla\\Component\\Content\\Site\\Helper\\RouteHelper') && Version::MAJOR_VERSION === 3) {
  require_once JPATH_ROOT . '/components/com_content/helpers/route.php';
  class_alias('ContentHelperRoute', '\\Joomla\\Component\\Content\\Site\\Helper\\RouteHelper');
}


/** @var $item */
/** @var string $moduleclass_sfx */

$params = $item->params;
HTMLHelper::addIncludePath(JPATH_BASE . '/components/com_content/helpers');
$canEdit = $params->get('access-edit');
$info = $params->get('info_block_position', 0);
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));
?>
<div class="n3tArticle">
<?php if ($params->get('show_title')) : ?>
  <<?php echo $params->get('article_header_tag', 'h4')?> class="item-title">
    <?php if ($params->get('link_titles') && ($params->get('access-view') || $params->get('show_noauth', '0') == '1')) : ?>
      <a href="<?php echo Route::_(RouteHelper::getArticleRoute($item->slug, $item->catid, $item->language)); ?>">
        <?php echo $item->title; ?></a>
    <?php else : ?>
      <?php echo $item->title; ?>
    <?php endif; ?>
  </<?php echo $params->get('article_header_tag', 'h4')?>>
<?php endif; ?>

<?php if ($canEdit) : ?>
  <?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $item, 'print' => false)); ?>
<?php endif; ?>

<?php if ($params->get('show_tags') && !empty($item->tags->itemTags)) : ?>
  <?php echo LayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
<?php endif; ?>

<?php $useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
  || $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author') ); ?>

<?php if ($useDefList && ($info == 0 || $info == 2)) : ?>
  <?php echo LayoutHelper::render('joomla.content.info_block.block', array('item' => $item, 'params' => $params, 'position' => 'above')); ?>
  <?php if ($info == 0 && $params->get('show_tags', 1) && !empty($item->tags->itemTags)) : ?>
    <?php echo LayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
  <?php endif; ?>
<?php endif; ?>

<?php echo LayoutHelper::render('joomla.content.intro_image', $item); ?>

<?php if (!$item->params->get('show_intro')) : ?>
  <?php echo $item->event->afterDisplayTitle; ?>
<?php endif; ?>

<?php echo $item->event->beforeDisplayContent; ?>

<div class="item-text">
  <?php echo $item->text; ?>
</div>

<?php if ($useDefList && ($info == 1 || $info == 2)) : ?>
  <?php echo LayoutHelper::render('joomla.content.info_block.block', array('item' => $item, 'params' => $params, 'position' => 'below')); ?>
  <?php if ($params->get('show_tags', 1) && !empty($item->tags->itemTags)) : ?>
    <?php echo LayoutHelper::render('joomla.content.tags', $item->tags->itemTags); ?>
  <?php endif; ?>
<?php endif; ?>

<?php if ($params->get('show_readmore') && $item->readmore) :
  if ($item->params->get('access-view')) :
    $link = Route::_(RouteHelper::getArticleRoute($item->slug, $item->catid, $item->language));
  else :
    $menu = Factory::getApplication()->getMenu();
    $active = $menu->getActive();
    $itemId = $active->id;
    $link = new Uri(Route::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false));
    $link->setVar('return', base64_encode(RouteHelper::getArticleRoute($item->slug, $item->catid, $item->language)));
  endif; ?>

  <?php echo LayoutHelper::render('joomla.content.readmore', array('item' => $item, 'params' => $params, 'link' => $link)); ?>

<?php endif; ?>

<?php echo $item->event->afterDisplayContent; ?>

</div>
