n3t Article
===========

![Version](https://img.shields.io/badge/Latest%20version-5.0.1-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--02--22-informational)

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-v7.4-green)][PHP]
[![PHP](https://img.shields.io/badge/PHP-v8.x-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-article/badge/?version=latest)][DOCS]

n3t Article is simple module, allowing display one selected article in any module 
position.

Documentation
-------------

Find more at [documentation page](http://n3tarticle.docs.n3t.cz/en/latest/)

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[DOCS]: http://n3tarticle.docs.n3t.cz/en/latest/
